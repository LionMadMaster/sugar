## PSR2Smart code style standard for CodeSniffer


### Official CodeSniffer Documentation

Documentation for the CodeSniffer can be found on the [PEAR website](http://pear.php.net/package/PHP_CodeSniffer).


### Install standard

Copy or create a symbolic link contents of a folder `Standards` in the directory `CODE_SNIFFER_PATH/Standards`
Folder `CODE_SNIFFER_PATH/Standards` also should contain subdirectories `PSR1` and `PSR2`

#### Standards folder examples:

1. In Max OS: `/usr/local/Cellar/php55/5.5.12/lib/php/PHP/CodeSniffer/Standards/`
2. In Ubuntu/Xubuntu `/usr/share/php/PHP/CodeSniffer/Standards/`
3. ...

### Configure IDE

1. [PhpStorm](https://www.jetbrains.com/phpstorm/help/using-php-code-sniffer-tool.html)
2. ...

### Set autovalidate before commit

http://github.com/s0enke/git-hooks